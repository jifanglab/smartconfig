export default {
	apiBaseUrl:'https://test.xingweixian.com/func',
	//apiBaseUrl:'https://demo.xingweixian.com/func', //体验版的接口地址，每次上传体验版的时候记得改到这个
	//front 页面进来获取到的deviceid 用于比如注册之后返回页面获取数据
	deviceid:'',
	
	//登录参数
	accounttype: 101,
	pass: 'xwx',
	//登录后信息
	employee:{},
	//登录后权限id数组
	actions:[] ,
	
	//用户授权获取的临时头像路径
	avatarUrl:'',
	
	//扫码添加设备的时候 查询到的设备信息 首页切换设备也会被赋值到这里
	deviceinfo:{},
	
	//绑定设备的时候的类型
	deviceBindType:{
		BIND_WARDS:1302209,//绑定被监护人
		BIND_ADMIN:1302207,//绑定管理员
	},
	
	//被监护人的事件类型
	wardsEventType:{
		TURN_OVER:100, //翻身
		DEFECATE_PEE:101, //大小便 就是告警
		REPLACE_DIAPER:102,//更换尿片
		BIND_DIAPER:103,//绑定尿片
		HEART_BEAT:105,//心跳
		RESET:104 //重置
	},
	
	prevPath:'' ,//记录上一个页面的地址，方便跳转, 暂时没用	
	
	hasShowLogin:false, //是否已经展示了登录界面，如果已经展示了，表示用户取消了登录，那么没有登录的界面也能看
	hasLogin:false, //标记用户是否登录 用与页面判断是否该执行登录后的数据请求
	
	/**
	* 	101护工需更换尿片提醒
		102护工需更换尿片警告
		103护工已更换尿片

		111需更换尿片提醒
		112需更换尿片警告
		113已更换尿片
		
		103 和113 合并为更换记录 
		103 合并到113 
		103 作废
	 */
	messageType:{
		NURSE_NOTICE:101,
		NURSE_WARNING:102,
		NURSE_DONE:103,
		NOTICE:111,
		WARNING:112,
		DONE:113
	}
}