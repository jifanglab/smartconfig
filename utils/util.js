
const warning = (message)=>{
	uni.showToast({
		title:message,
		icon:'none'
	})
}

const success = (message)=>{
	uni.showToast({
		title:message
	})
}

const rpxTopx = (rpx)=>{
	return rpx * wx.getSystemInfoSync().windowWidth / 750;
}

  
  export function setNull(val){
    if(val == null || val == '' || val == undefined || val == 'undefined'){
        return '';
    }else{
        return val;
    }
  }

  export function isNull(val){
      if(val == null || val == '' || val == undefined || val == 'undefined'){
          return true;
      }else{
          return false;
      }
  }

  export function isNotNull(val){
    if(val == null || val == '' || val == undefined || val == 'undefined'){
        return false;
    }else{
        return true;
    }
  }
  
  export function isEmptyObj(obj){
	  if(JSON.stringify(obj) === '{}' || !obj ) return true;
	  else return false;
  }
  

export default {
	warning,
	success,
	rpxTopx,
	setNull,
	isNull,
	isNotNull,
	isEmptyObj	
}