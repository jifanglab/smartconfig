//获取今天日期-初始化日期
export function getTodayDate(format) {
	var nowDate = new Date();
	var year = nowDate.getFullYear(),
		month = nowDate.getMonth() + 1,
		day = nowDate.getDate()
	var formatDate = format ? format.replace('yyyy', year).replace('MM', month).replace('dd', day) : year + '-' +
		month + '-' + day;

	return formatDate;
}


export function getFormatDate(format, nowDate) {
	// var nowDate = new Date(choseDate);
	var year = nowDate.getFullYear(),
		month = nowDate.getMonth() + 1,
		day = nowDate.getDate();
	return format.replace('yyyy', year).replace('MM', month).replace('dd', day);
}

/**
 * 把带T的时间转为正常的时间格式 yyyy-mm-dd hh:mm:ss
 * @param {String} time 带T时间
 */
export function timeFormatter(time) {
	if (!time) return time;
	var date = time.substr(0, 10); //年月日
	var hours = time.substring(11, 13);
	var minutes = time.substring(14, 16);
	var seconds = time.substring(17, 19);
	var timeFlag = date + ' ' + hours + ':' + minutes + ':' + seconds;
	timeFlag = timeFlag.replace(/-/g, "/");
	timeFlag = new Date(timeFlag);
	timeFlag = new Date(timeFlag.getTime() + 8 * 3600 * 1000);
	timeFlag = timeFlag.getFullYear() + '-' + ((timeFlag.getMonth() + 1) < 10 ? "0" + (timeFlag.getMonth() + 1) : (
			timeFlag.getMonth() + 1)) + '-' + (timeFlag.getDate() < 10 ? "0" + timeFlag.getDate() : timeFlag
			.getDate()) + ' ' + (timeFlag.getHours() < 10 ? "0" + timeFlag.getHours() : timeFlag.getHours()) + ':' +
		(timeFlag.getMinutes() < 10 ? "0" + timeFlag.getMinutes() : timeFlag.getMinutes()) + ':' + (timeFlag
			.getSeconds() < 10 ? "0" + timeFlag.getSeconds() : timeFlag.getSeconds());
	return timeFlag;
}


//减月份 
export function subtractMonth(choseDate, monthNum) {
	// var selectday = choseDate.replace(/-/g,"/");//替换字符，变成标准格式;
	// var date = new Date(selectday); //转换为时间
	var date = new Date(new Date(Date.parse(choseDate)).setMonth((new Date(Date.parse(choseDate)).getMonth() -
		monthNum)));
	console.log(date);
	var changeDay;

	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();

	changeDay = getDateTime(day, month, year)

	//判断日期
	changeDay = panduanData(changeDay) ? changeDay : choseDate;
	return changeDay;
}
//加月份
export function addMonth(choseDate, monthNum) {
	var date = new Date(new Date(Date.parse(choseDate)).setMonth((new Date(Date.parse(choseDate)).getMonth() +
		monthNum)));
	console.log(date);
	var changeDay;

	var year = date.getFullYear();
	var month = date.getMonth() + 1;
	var day = date.getDate();

	changeDay = getDateTime(day, month, year)

	//判断日期
	changeDay = panduanData(changeDay) ? changeDay : choseDate;
	return changeDay;
}

// 拼接时间
export function getDateTime(day, month, year) {
	var changeDay;
	if (day < 10) {
		if (month < 10) {
			changeDay = year + '-0' + month + '-0' + day
		}
		if (month >= 10) {
			changeDay = year + '-' + month + '-0' + day
		}
	}
	if (day >= 10) {
		if (month < 10) {
			changeDay = year + '-0' + month + '-' + day
		}
		if (month >= 10) {
			changeDay = year + '-' + month + '-' + day
		}
	}
	//判断日期
	// panduanData(changeDay)
	return changeDay;
}
/**
 * 
 * @param {*} times  时间戳
 * 转换为  yyyy-MM-dd HH:MM:SS 格式的日期
 */
export function formatDateTohms(times) {
	var date = new Date(times);
	var year = date.getFullYear(); //年份
	var month = date.getMonth() + 1; //月份
	var day = date.getDate(); //日
	var hour = function() { //获取小时
		return date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
	}
	var minute = function() { //获取分钟
		return date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
	}

	var second = function() { //获取秒数
		return date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
	}
	return year + '-' + month + '-' + day + ' ' + hour() + ':' + minute() + ':' + second()

}

export function formatDateTohm(times) {
	var date = new Date(times);
	var year = date.getFullYear(); //年份
	var month = date.getMonth() + 1; //月份
	var day = date.getDate(); //日
	var hour = function() { //获取小时
		return date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
	}
	var minute = function() { //获取分钟
		return date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
	}

	var second = function() { //获取秒数
		return date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
	}
	return hour() + ':' + minute();

}

export function getHour(times) {
	var date = new Date(times);
	var year = date.getFullYear(); //年份
	var month = date.getMonth() + 1; //月份
	var day = date.getDate(); //日
	var hour = function() { //获取小时
		return date.getHours() < 10 ? '0' + date.getHours() : date.getHours()
	}
	var minute = function() { //获取分钟
		return date.getMinutes() < 10 ? '0' + date.getMinutes() : date.getMinutes();
	}

	var second = function() { //获取秒数
		return date.getSeconds() < 10 ? '0' + date.getSeconds() : date.getSeconds();
	}
	return hour();
}


//判断日期
export function panduanData(changeDay) {
	//获取今日时间戳
	var nowDate = new Date();
	var year = nowDate.getFullYear(),
		month = nowDate.getMonth() + 1,
		day = nowDate.getDate();
	var nowDay = `${year}/${month}/${day}`;
	var today = new Date(Date.parse(nowDay)).getTime();
	//选择的日期
	var selectday = changeDay.replace(/-/g, "/");
	var selectdDate = new Date(Date.parse(selectday)).getTime();
	//判断选择日期与今日的大小
	if (selectdDate > today) {
		uni.showToast({
			title: '只能查看今日及以前的记录',
			icon: 'none',
			duration: 2000 //提示展示3秒关闭
		})
		return
	} else {
		// this.setData({
		//     choseDate: changeDay //选择日期赋值
		// })
		// this.panDuanWeek() //判断星期几
	}

}

//获取某月的天数
export function getMonthDays(date) {
	let now = new Date(date);
	let nowYear = now.getYear(); //当前年 
	let nowMonth = now.getMonth(); //当前月 
	let monthStartDate = new Date(nowYear, nowMonth, 1);
	let monthEndDate = new Date(nowYear, nowMonth + 1, 1);
	let days = (monthEndDate - monthStartDate) / (1000 * 60 * 60 * 24);
	return days;
}

//获取今天开始的时间 yyyy-MM-dd HH:MM:SS 格式的日期
export function getTodayBegin() {
	var nowDate = new Date();
	var year = nowDate.getFullYear(),
		month = nowDate.getMonth() + 1,
		day = nowDate.getDate()
	return year + '-' + month + '-' + day + ' ' + '00:00:00';
}

export default {
	getTodayDate,
	getFormatDate,
	formatDateTohms,
	formatDateTohm,
	timeFormatter,
	subtractMonth,
	addMonth,
	getMonthDays,
	getTodayBegin
}
